## context

Lodash is a utility library that provides common functionality. But in some places we should not use lodash due to space or some other constrain.

So we planed to have assignment on commonly used functions in lodash. You need to implement that functionality on your own with plan JS

for example 

``` js
// https://lodash.com/docs/#compact
import { compact } from 'lodash'

compact([0, 1, 2])
// => [1, 2]
```

 written in plan JS
 ```js
[0, 1, 2].filter(x => !!x)
// => [1, 2]
 ```

## Assignment instructions

- you need write your custom service in `services.js`
- You can reuse existing service if you need in your other service.
- All the services should be imported in to `index.js` and execute the service
- here structure look like. for this i am using uniq function to implement 
  
  ```js
    // https://lodash.com/docs/4.17.15#uniq
    console.log("Uniq"); // service name
    const uniqInput = [1,1,3,42,4,5]
    console.log("Service Input",uniqInput); 
    const uniqResponse = uniq(uniqInput)
    console.log("Lodash output",uniqResponse);
    const customUniqResponse = customUniq(uniqInput)
    console.log("customUniqResponse output",customUniqResponse);
  ```
- For the function instructions how it is used see the [lodash](https://lodash.com/docs/4.17.15) documentation.

## Requirements for this assignment

- [ ] [node](https://nodejs.dev/learn/how-to-install-nodejs)
- [ ] [lodash](https://lodash.com/docs/4.17.15)

## ⚠️ Caution ❌

###  As this is popular library you can find this online. please avoid that. 

## Service to implement

- [ ] uniq
- [ ] filter
- [ ] difference
- [ ] intersection
- [ ] remove
- [ ] unionBy
- [ ] uniqBy
- [ ] find
- [ ] keyBy
- [ ] reduce
- [ ] groupBy
- [ ] reject
- [ ] sortBy
- [ ] debounce
- [ ] delay
- [ ] isNaN
- [ ] isBoolean
- [ ] max
- [ ] omitBy
- [ ] pickBy

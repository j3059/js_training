/**
 * Problem: 
 *  Given a file path, display the contents of the file in the following format
 * 
 * ref: https://www.geeksforgeeks.org/node-js-fs-readfile-method/
 * 
   Format:

    File Name: <Only-File-Name>
    Contents:
    <Contents>
    ===


    1) node cat.js filePath
        Print the file contents in the above format
    2) node cat.js filePath --top:n
        Given a file path, print top n lines in the above format
    3) node cat.js filePath --bottom:n
        Given a file path, print bottom n lines in the above format
 */

const readInput = () => {
  let fileName, option, optionVal;

  // TODO: write your code here to fetch data from command line arguments
  // Run command: node cat.js file.txt --top:5

  return { fileName, option, optionVal };
};

const printFileContents = (content) => {
  // TODO: Write console.log's here
};

// This function takes arguments and return another function
const getFileContents = (option, optionVal) => {
  return (err, content) => {
    // Here you can access option and optionVal
    // Process content here
  };
};

const processInput = ({ fileName, option, optionVal }) => {
  // TODO: Write your code here to print the file contents based on options given
  /**
   *  1) open the file
   *  2) use getFileContents callback to read the file contents
   *     2.1) Call the getFileContents like below and then you can access options
   *          fs.readFile(filename, getFileContents(option, optionVal))
   *  3) use printFileContents to print content
   * */
};

const main = () => {
  const { fileName, option, optionVal } = readInput();
  processInput({ fileName, option, optionVal });
};

main();

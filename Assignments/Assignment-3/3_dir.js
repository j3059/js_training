/**
 * Problem:
 *  1) Given a folder path,
 *      print the list of it's children (non-recursive) with n items in a row
    2) Given a folder path,
        print the list of it's children (recursive) with an item in each row in the following format

  Format:

    Folder Name: <Only-Folder-Name>
    Contents:
    Total Files & Folders: 5
    1. File1.txt
    2. folder1/File2.txt
    3. folder1/File3.js
    4. folder2/File4.js
    5. <and-so-on>
    ===
 */

/**
 * Problem:
 * Given a file path, implement a persistent tail command (same as what any shell does) 
 * that uses file watching.

 * * If the file is deleted, then you should stop watching

 * ref: https://www.geeksforgeeks.org/node-js-fs-watch-method/

 * Output:
  ** if it is file path

    Listening at .../.../file.ext
    file changed
    file changed
    file renamed
    .....

  ** if it is folder path

    Listening at .../directory

    Path: cat.js - rename - on: 23/May/2022 7:39:34
    Path: sub/dir.js - change - on: 23/May/2022 7:42:45
    .....
 */

/**
 * Problem:
 * Given a list of files in an array, 
 * the number of lines to read in each file in an array and the result file,
 * read the files parallelly and assimilate the corresponding number of lines in each file
 * in the same order & store it onto a new file called aggregatedContents.txt in the following format
 *
 * 
  Format:
    # Contents of aggregatedContents.txt
    File: <Full-File1-Path>
    <file1 lines>
    ==
    File: <Full-File2-Path>
    <file2 lines>
    ==
    # and son

    Note: Same as prev one use callbacks to read file contents and write the lines count into resultant file
 */

/**
 * Problem:
 * use fetch() to make network calls
 *
 * API: http://api.weatherapi.com/v1/current.json?key=<YOUR_API_KEY>&q=<ANY_PLACE>
 *
 * Get API_KEY from https://www.weatherapi.com/ site by register with your credentials

 return response in the following format:

 {
     statusCode : 200,
     data:{
            name: "Visakhapatnam",
            region: "AP",
            country: "India",
            last_updated: "2022-05-25 00:45",
            "temp_c": 28.9,
            "temp_f": 84.0,
            "wind_kph": 20.9,
            "humidity": 77,
        }
    }
 */

## Tilicho JS Training

### Submission Process

- You need to create your own branch from master.
- The branch name should follow this formate `<year>/intern/<your name>`. ex :  `2022/intern/naveen`
- You need to create a solutions folder in that.
- Copy the Assignment form assignments folder to solutions folder and start work. 

> This how the your folder structure look like with assignment 1 solution

![Solution Submission](./solution_submission.png?raw=true)
> ### ❌ You should not merge the solution to main.

